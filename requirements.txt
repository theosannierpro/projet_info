InquirerPy==0.3.4
pandas==2.2.2
pytest==8.1.1


# Pour mettre à jour ce document automatiquement : installer le package pipreqs
# et taper la commande :
# pipreqs . --force