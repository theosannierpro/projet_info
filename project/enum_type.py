"""Ce module implémente la classe Enum_type"""

from enum import Enum


class EnumType(Enum):
    """ Enumération des différents types de biens immobiliers.

    Attributes
    -------------------------
    Maison : int
        Type de bien : Maison.
    Appartement : int
        Type de bien : Appartement.
    Dependance : int
        Type de bien : Dépendance.
    Local_Commercial : int
        Type de bien : Local Commercial.
    """
    Maison = 1
    Appartement = 2
    Dependance = 3
    Local_Commercial = 4
