"""Ce module implémente la classe Client"""

import pandas as pd
from colorama import Fore, Style
from project.personne import Personne
from project.generateurs_id import generate_id_client


class Client(Personne):
    """Représente un client.

    Attributes
    -------------------------
    prenom : str
        Prénom du client.
    nom : str
        Nom du client.
    age : int
        Âge du client.
    id_client : str
        Identifiant (supposé unique) du client.

    Methods
    -------------------------
    get_annonces_client() -> pandas.DataFrame
        Récupère toutes les annonces associées à ce client dans la base de données
        des annonces.

    creer_compte() -> None
        Crée un nouveau compte client et sauvegarde les informations dans la base de données.
    """
    def __init__(self,
                 prenom: str,
                 nom: str,
                 age: int,
                 id_client: str,
                 ):
        super().__init__(prenom, nom, age)
        self.id_client = id_client
        if not isinstance(id_client, str):
            raise TypeError(
                "L'identifiant client doit être une instance de string."
                )

    def get_annonces_client(self):
        """ Récupère toutes les annonces associées à ce client.
        
        Effectue un filtrage de la base de données des annonces sur les identifiants
        clients pour ne conserver que les annonces dont le client est propriétaire.

        Returns
        -------------------------
        pandas.DataFrame
            DataFrame contenant toutes les annonces du client.

        Examples
        --------------------------
        >>> client = Client('John', 'Doe', 30, 'ABC123')
        >>> annonces = client.get_annonces_client()
        >>> print(annonces)
            ... (contenu du DataFrame)
        """
        bdd_annonces = pd.read_csv('resources/base_de_donnees.csv')
        id_user = self.id_client
        return bdd_annonces.query('Client == @id_user')

    def creer_compte(self):
        """ Crée un nouveau compte client
        
        Crée un nouveau compte client et sauvegarde les informations dans la base de données.

        Returns
        -------------------------
        None

        Examples
        --------------------------
        >>> client = Client('John', 'Doe', 30, '')
        >>> client.creer_compte()
        Votre compte a été créé avec succès ! Votre identifiant est ABCD
        """
        new_id = generate_id_client()
        self.id_client = new_id
        new_row_bdd = pd.DataFrame({
                'prenom': [self.prenom],
                'nom': [self.nom],
                'age': [int(self.age)],
                'id_client': [new_id]})
        # Convertir les données de la nouvelle ligne en chaîne CSV
        new_line = '\n'+','.join(map(str, new_row_bdd.iloc[0].tolist()))
        with open('resources/bdd_client.csv', 'a', encoding='utf-8') as f:
            # On ajoute une nouvelle ligne au csv existant
            f.write(new_line)
        print(Fore.GREEN + f"Votre compte a été créé avec succès ! Votre identifiant est {self.id_client}" + Style.RESET_ALL)
