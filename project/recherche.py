"""Ce module implémente la classe Recherche, nécessaire à la recherche
d'annonces sur l'application"""

import re
from datetime import datetime
import pandas as pd
from InquirerPy import prompt, inquirer
from project.annonce import Annonce
from project.enum_type import EnumType


class Recherche:
    """Classe pour rechercher une annonce dans "base_de_donnees".

    Une instance de la classe Recherche est caractérisée par des critères et
    par un dataframe.

    Parameters
    ----------
    criteres : dict
        Dictionnaire stockant les critères associés à la recherche.

    df : DataFrame
        Base de données dans laquelle laquelle la recherche est effectuée.
        Ici, il s'agit de la base de données contenant les annonces.

    Methods
    ----------
    reset() -> None
        Réinitialise le dictionnaire de critères de recherche.
    affichage() -> None
         Affiche les 10 premières lignes du DataFrame des annonces.
    ajouter_critere() -> None
        Ajoute un critère au dictionnaire des critères de filtrage
    recherche() -> pandas.DataFrame
        DataFrame contenant les résultats de la recherche en fonction des
        critères.
    lancer_recherche() -> None
        Recherche les lignes du dataframe selon certains critères.
    """
    def __init__(self):
        BDD_annonces = pd.read_csv('resources/base_de_donnees.csv')
        self.criteres = {}
        self.df = BDD_annonces

    def reset(self):
        """
        Réinitialise le dictionnaire de critères de recherche.
        """
        self.criteres = {}

    def affichage(self):
        """
        Affiche les 10 premières lignes du DataFrame des annonces.
        Et plus si besoin.
        """
        num_lignes = 10
        index = 0

        while index < len(self.df):
            if index >= len(self.df):
                print("Il n'y a plus de résultats à afficher.")
                break

            # Afficher les 10 lignes suivantes
            print(self.df.iloc[index:index + num_lignes])

            options = ["Afficher plus de résultats",
                       "Retour au menu de recherche"]
            choix = inquirer.select(
                message="Que voulez-vous faire?",
                choices=options,
                default="Retour au menu de recherche"
            ).execute()

            if choix == "Retour au menu de recherche":
                break

            index += num_lignes

    def ajouter_critere(self):
        """
        Permet à l'utilisateur d'ajouter un critère de recherche.

        Utilise un prompt interactif pour demander quel critère ajouter,
        puis demande une valeur pour ce critère. Si des erreurs surviennent,
        invite l'utilisateur à réessayer. Ajoute le critère et sa valeur au
        dictionnaire `self.criteres`.
        """
        questions = [
            {
                "type": "list",
                "name": "critere",
                "message": "Selon quel critère voulez-vous rechercher une "
                "annonce ?",
                "choices": [
                    "Valeur du bien",
                    "Date de mise en vente",
                    "Code postal",
                    "Nombre de pièces",
                    "Surface du bien",
                    "Surface du terrain",
                    "Référence de l'annonce",
                    "Type local"
                ],
            },
        ]
        answers = prompt(questions)

        colonne = answers["critere"]

        if colonne == "Type local":
            colonne = "Type_local"
            questions_type = [
                {
                    "type": "list",
                    "name": "Type_local",
                    "message": "Quel type local ?",
                    "choices": [
                        "Dépendance",
                        "Maison",
                        "Appartement",
                        "Local commercial"
                    ],
                },
            ]
            answers_type = prompt(questions_type)

            if answers_type["Type_local"] == "Dépendance":
                valeur = "Dependance"
            elif answers_type["Type_local"] == "Local commercial":
                valeur = "Local_Commercial"
            else:
                valeur = answers_type["Type_local"]

            self.criteres[colonne] = valeur
            # Ajoute au dictionnaire des critères

        else:
            while True:
                try:
                    questions_valeur = [
                        {
                            "type": "input",
                            "name": "valeur",
                            "message": "Entrez la valeur correspondante ."
                        },
                    ]
                    answers_valeur = prompt(questions_valeur)
                    valeur = answers_valeur["valeur"]

                    if colonne == "Date de mise en vente":
                        colonne = "Date_mutation"
                        datetime.strptime(valeur, "%Y-%m-%d")
                        # Valide le format de date

                    elif colonne == "Code postal":
                        colonne = "Code_postal"
                        pattern = r'^\d{2}_\d{3}$'
                        if not re.match(pattern, valeur):
                            raise ValueError("Ce n'est pas le bon format, "
                                             "essayez XX_XXX.")

                    elif colonne == "Valeur du bien":
                        colonne = "Valeur_fonciere"
                        valeur = float(valeur)
                        if valeur < 0:
                            raise ValueError("La valeur du bien doit être "
                                             "positive.")

                    elif colonne == "Surface du bien":
                        colonne = "Surface_reelle_bati"
                        valeur = float(valeur)
                        if valeur < 0:
                            raise ValueError("La surface du bien doit être "
                                             "positive.")

                    elif colonne == "Surface du terrain":
                        colonne = "Surface_terrain"
                        valeur = float(valeur)

                    elif colonne == "Nombre de pièces":
                        colonne = "Nombre_pieces_principales"
                        valeur = int(valeur)

                    elif colonne == "Référence de l'annonce":
                        colonne = "ref_annonce"

                    self.criteres[colonne] = valeur
                    # Ajoute au dictionnaire des critères
                    break  # Sortie de la boucle quand c'est réussi

                except (ValueError, TypeError) as e:
                    print(f"Erreur : {e}. Veuillez réessayer.")

        print(self.criteres)

    def rechercher(self):
        """
        Effectue une recherche dans le DataFrame basée
        sur les critères ajoutés.

        Returns
        -------
        pd.DataFrame
            DataFrame contenant les résultats de la recherche en fonction des
            critères.
        """
        query = True  # Pour accumuler les conditions
        for col, valeur in self.criteres.items():
            query = query & (self.df[col] == valeur)
        return self.df[query]

    def lancer_recherche(self):
        if not self.criteres:
            print("Vous devez entrer au moins un critère pour lancer la "
                  "recherche.")
            return

        resultat = self.rechercher()

        if resultat.empty:
            print("Aucune annonce ne correspond à vos critères.")
            return

        num_lignes = 10
        index = 0
        while index < len(resultat):
            fin = min(index + num_lignes, len(resultat))

            print("Voici les annonces correspondantes:")
            print(resultat.iloc[index:fin])

            # Liste des options pour l'utilisateur
            options = ["Page suivante", "Afficher les détails",
                       "Retour au menu principal", "Page précédente"]

            choix = inquirer.select(
                message="Que voulez-vous faire?",
                choices=options,
                default="Afficher plus de résultats"
            ).execute()

            if choix == "Afficher les détails":
                # Demandez à l'utilisateur de sélectionner l'annonce
                # dont il veut les détails
                selections = [str(i) for i in range(index, fin)]
                selection = inquirer.select(
                    message="Sélectionnez une annonce pour voir les détails",
                    choices=selections
                ).execute()

                index_selected = index + int(selection)
                # Affiche les résultats de la
                # page courante

                ligne = resultat.iloc[index_selected]
                # Ici, on extrait les données nécessaires pour l'objet Annonce
                date_mise_en_vente = datetime.strptime(ligne['Date_mutation'],
                                                       "%Y-%m-%d").date()
                valeur_fonciere = float(ligne['Valeur_fonciere'])
                code_postal = str(ligne['Code_postal'])
                nb_pieces_principales = int(ligne['Nombre_pieces_principales'])
                surface_terrain = float(ligne['Surface_terrain'])
                surface_reelle_bati = float(ligne['Surface_reelle_bati'])
                type_bien = EnumType[ligne['Type_local']]
                ref_annonce = ligne['ref_annonce']
                statut = ligne['Statut']
                client = ligne['Client']

                annonce_selectionnee = Annonce(
                    date_mise_en_vente=date_mise_en_vente,
                    valeur_fonciere=valeur_fonciere,
                    code_postal=code_postal,
                    nb_pieces_principales=nb_pieces_principales,
                    surface_terrain=surface_terrain,
                    surface_reelle_bati=surface_reelle_bati,
                    type_bien=type_bien,
                    ref_annonce=ref_annonce,
                    statut=statut,
                    client=client
                )

                # Afficher les détails de l'annonce

                print(annonce_selectionnee.__str__())
                # print(afficher_details_annonce(annonce_selectionnee))

            elif choix == "Retour au menu principal":
                break

            elif choix == "Page suivante":
                index += num_lignes

            elif choix == "Page précédente":
                index -= num_lignes
