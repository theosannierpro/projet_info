"""Ce module implémente la view ClientView (menu principal du client)"""

from InquirerPy import prompt
from colorama import Fore, Style
from project.view.abstract_view import AbstractView


class ClientView(AbstractView):
    """View du menu principal du client

    Attributes
    -------------
        questions_menu : list
            Options du menu principal

    """
    def __init__(self):
        super().__init__()
        self.questions_menu = [
            {
                "type": "list",
                "name": "Menu principal",
                "message": "Que souhaitez-vous faire ?",
                "choices": [
                    "Faire estimer un bien",
                    "Poster une annonce",
                    "Consulter les annonces",
                    "Modifier une de mes annonces",
                    "Supprimer une de mes annonces",
                    "Me déconnecter",
                    "Quitter l'application",
                ],
            }
        ]

    def make_choice(self, user_info: str):
        """Gère le choix du client dans le menu principal.

        Returns
        --------------
        tuple
            Un tuple contenant la vue suivante et les informations sur le client.

        """
        answers_menu = prompt(self.questions_menu)
        if answers_menu["Menu principal"] == "Faire estimer un bien":
            from project.view.estim_view import EstimerView
            next_view = EstimerView()
        elif answers_menu["Menu principal"] == "Poster une annonce":
            from project.view.poster_view import PosterView
            next_view = PosterView()
        elif answers_menu["Menu principal"] == "Consulter les annonces":
            from project.view.rechercher_view import RechercheView
            next_view = RechercheView()
        elif answers_menu["Menu principal"] == "Modifier une de mes annonces":
            from project.view.modifier_view import ModifierView
            next_view = ModifierView()
        elif answers_menu["Menu principal"] == "Supprimer une de mes annonces":
            from project.view.supprimer_view import SupprimerView
            next_view = SupprimerView()
        elif answers_menu["Menu principal"] == "Me déconnecter":
            from project.view.accueil_view import AccueilView
            return [AccueilView(), '']
        elif answers_menu["Menu principal"] == "Quitter l'application":
            next_view = None
        else:
            from project.view.accueil_view import AccueilView
            return [AccueilView(), '']

        return [next_view, user_info]

    def display_info(self):
        print(Fore.BLUE + " MENU PRINCIPAL ".center(80, "=") + Style.RESET_ALL)
