"""Ce module implémente la view SupprimerView, ie. le menu pour supprimer une annonce."""

from InquirerPy import prompt
from colorama import Fore, Style
from project.view.abstract_view import AbstractView
from project.annonce import Annonce


class SupprimerView(AbstractView):
    """Classe de vue pour supprimer une annonce.

    Attributes
    -----------------
    question_confirm : list
        Question pour confirmer la suppression de l'annonce.

    """
    def __init__(self):
        super().__init__()
        self.question_confirm = [{
                "type": "confirm",
                "name": "Confirmer suppression",
                "message": "Etes vous sûr de vouloir supprimer cette annonce ?"
                "(Cette action est irréversible)"
            }]

    def make_choice(self, user_info: str):
        annonces_user = user_info.get_annonces_client()
        choix = []
        compteur = 1
        for index, ligne in annonces_user.iterrows():
            mise_en_page_ligne = f"{compteur}. {ligne['Date_mutation']} | {ligne['Type_local']} | {ligne['Valeur_fonciere']} | {ligne['ref_annonce']}"
            choix.append(mise_en_page_ligne)
            compteur += 1
        choix.append("Retour au menu principal")
        questions = [
            {
                "type": "list",
                "name": "Liste Annonces",
                "message": "Quelle annonce souhaitez vous supprimer ? \n"
                           " Date de publication | Type local |   Prix    | Ref. \n"
                           "----------------------------------------------------",
                "choices": choix,
            }]

        answers = prompt(questions)
        if answers['Liste Annonces'] == "Retour au menu principal":
            from project.view.client_view import ClientView
            return [ClientView(), user_info]
        else:
            answer_confirm = prompt(self.question_confirm)
            if answer_confirm['Confirmer suppression']:
                Annonce.supprimer_annonce(answers['Liste Annonces'].split("|")[-1].strip())
            return [SupprimerView(), user_info]

    def display_info(self):
        print(Fore.BLUE + "MODIFICATION D'UNE ANNONCE".center(80, "=") + Style.RESET_ALL)
