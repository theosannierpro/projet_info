"""Ce module implémente la view des Agents immobiliers"""

from project.view.abstract_view import AbstractView
from project.view.accueil_view import AccueilView


class AgentImmoView(AbstractView):
    """Classe représentant la vue pour les agents immobiliers."""
    def __init__(self):
        super().__init__()

    def make_choice(self, user_info: str):
        return [AccueilView(), '']

    def display_info(self):
        print("L'application ne vous est pas encore accessible. Un peu de patience!")
