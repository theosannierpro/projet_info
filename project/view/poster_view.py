"""Ce module implémente la view PosterView, ie. le menu pour poster une annonce."""

from datetime import date
from InquirerPy import prompt
from colorama import Fore, Style
from project.view.abstract_view import AbstractView
from project.annonce import Annonce
from project.enum_type import EnumType
from project.generateurs_id import generate_id_annonce


class PosterView(AbstractView):
    """Classe de la view pour la création et la publication d'une annonce.

    Attributes
    ------------------------
    questions_creation : list
        Questions pour la création de l'annonce.
    question_validation : list
        Question pour la validation des informations de l'annonce.
    question_insatisfait : list
        Question pour les actions possibles si l'utilisateur souahaite modifier
        les informations renseignées.
    """
    def __init__(self):
        super().__init__()
        self.questions_creation = [
            {"type": "input",
             "name": "valeur foncière",
             "message": "Quel prix de vente souhaitez vous fixer pour votre bien ?"},
            {"type": "input",
             "name": "code postal",
             "message": "Quel est le code postal de votre bien ?"},
            {"type": "input",
             "name": "nb pieces principales",
             "message": "Quel est le nombre de pieces principales de votre bien ?"},
            {"type": "input",
             "name": "surface terrain",
             "message": "Quelle est la surface du terrain de votre bien ? (0 si pas de terrain)"},
            {"type": "list",
             "name": "type bien",
             "message": "Quel type de bien vendez-vous ?",
             "choices": ["Maison",
                         "Appartement",
                         "Dependance",
                         "Local_Commercial"]},
            {"type": "input",
             "name": "surface reelle bati",
             "message": "Quelle est la surface de votre bien ?"}
        ]
        self.question_validation = [
            {"type": "confirm",
                     "name": "validation",
                     "message": "Validez-vous ces informations ?"}]
        self.question_insatisfait = [
            {"type": "list",
                     "name": "insatisfait",
                     "message": "Que souhaitez-vous faire ?",
                     "choices": ["Recommencer la création d'une annonce",
                                 "Retour au menu principal"]}]

    def make_choice(self, user_info: str):
        answers = prompt(self.questions_creation)
        transfo_code_postal = answers["code postal"][:2] + '_' + answers["code postal"][2:]
        annonce_a_creer = Annonce(
                    date_mise_en_vente=date.today(),
                    valeur_fonciere=float(answers["valeur foncière"]),
                    code_postal=str(transfo_code_postal),
                    nb_pieces_principales=int(answers["nb pieces principales"]),
                    surface_terrain=float(answers["surface terrain"]),
                    surface_reelle_bati=float(answers["surface reelle bati"]),
                    type_bien=EnumType[str(answers["type bien"])],
                    ref_annonce=generate_id_annonce(),
                    statut='A vendre',
                    client=user_info.id_client)
        print(annonce_a_creer)
        answer_validation = prompt(self.question_validation)
        if answer_validation["validation"]:
            annonce_a_creer.poster_annonce()
        else:
            answer_insatisfait = prompt(self.question_insatisfait)
            if answer_insatisfait["insatisfait"] == "Recommencer la création d'une annonce":
                return [PosterView(), user_info]
            elif answer_insatisfait["insatisfait"] == "Retourner au menu principal":
                from project.view.client_view import ClientView
                return [ClientView(), user_info]
        from project.view.client_view import ClientView
        return [ClientView(), user_info]

    def display_info(self):
        print(Fore.BLUE + " CREATION D'UNE ANNONCE ".center(80, "=") + Style.RESET_ALL)
