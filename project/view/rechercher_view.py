"""Ce module implémente la view RechercheView, ie. le menu de recherche d'annonces."""

from InquirerPy import prompt
from colorama import Fore, Style
from project.view.abstract_view import AbstractView
from project.recherche import Recherche
from project.view.client_view import ClientView


class RechercheView(AbstractView):
    """Classe de vue pour effectuer une recherche d'annonce.

    Attributes
    -----------------
    recherche_instance : Recherche
        Instance de la classe Recherche pour effectuer la recherche.
    questions : list
        Question sur les options de la recherche.
    """
    def __init__(self):
        super().__init__()
        self.recherche_instance = Recherche()
        self.questions = [
            {
                "type": "list",
                "name": "Menu recherche",
                "message": "Que voulez vous faire ?",
                "choices": [
                    "Ajouter un critère",
                    "Lancer la recherche",
                    "Réinitialiser les critères",
                    "Afficher la base de données",
                    "Quitter le menu recherche"
                    ]
            }
        ]

    def make_choice(self, user_info: str):
        # Menu de recherche pour interagir avec l'utilisateur
        choix = prompt(self.questions)

        if choix["Menu recherche"] == "Ajouter un critère":
            self.recherche_instance.ajouter_critere()
            return [self, user_info]
        elif choix["Menu recherche"] == "Lancer la recherche":
            self.recherche_instance.lancer_recherche()
            return [self, user_info]
        elif choix["Menu recherche"] == "Quitter le menu recherche":
            # On reset les critères de recherche
            self.recherche_instance.reset()
            print("Vous quittez le menu recherche.")
            next_view = ClientView()
            return [next_view, user_info]
        elif choix["Menu recherche"] == "Réinitialiser les critères":
            self.recherche_instance.reset()
            return [self, user_info]
        elif choix["Menu recherche"] == "Afficher la base de données":
            self.recherche_instance.affichage()
            return [self, user_info]

    def display_info(self):
        print(Fore.BLUE + " MENU DE RECHERCHE ".center(80, "=") + Style.RESET_ALL)
