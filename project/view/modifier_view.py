"""Ce module implémente la view ModifierView, ie. le menu de modification d'une
annonce."""

from InquirerPy import prompt
from colorama import Fore, Style
from project.view.abstract_view import AbstractView
from project.annonce import Annonce


class ModifierView(AbstractView):
    """Classe de la view pour la modification d'une annonce.

    Attributes
    ---------------
    question_confirm_modif_cp : list
        Question pour la confirmation de la modification du code postal.
    question_modif_cp : list
        Question pour la modification du code postal.
    question_confirm_modif_prix : list
        Question pour la confirmation de la modification du prix de vente.
    question_modif_prix : list
        Question pour la modification du prix de vente.
    question_confirm_modif_type : list
        Question pour la confirmation de la modification du type de bien.
    question_modif_type : list
        Question pour la modification du type de bien.
    question_confirm_modif_pprincipales : list
        Question pour la confirmation de la modification du nombre de pièces principales.
    question_modif_pprincipales : list
        Question pour la modification du nombre de pièces principales.
    question_confirm_modif_terrain : list
        Question pour la confirmation de la modification de la surface de terrain.
    question_modif_terrain : list
        Question pour la modification de la surface de terrain.
    question_confirm_modif_surface : list
        Question pour la confirmation de la modification de la surface réelle du bâti.
    question_modif_surface : list
        Question pour la modification de la surface réelle du bâti.

    """
    def __init__(self):
        super().__init__()
        self.question_confirm_modif_cp = [
            {"type": "confirm",
                     "name": "confirm_cp",
                     "message": 'Souhaitez-vous modifier le code postal ?'}]
        self.question_modif_cp = [
            {"type": "input",
                     "name": "modif_cp",
                     "message": "Veuillez entrer le nouveau code postal:"}]
        self.question_confirm_modif_prix = [
            {"type": "confirm",
                     "name": "confirm_prix",
                     "message": 'Souhaitez-vous modifier le prix de vente ?'}]
        self.question_modif_prix = [
            {"type": "input",
                     "name": "modif_prix",
                     "message": "Veuillez entrer le nouveau prix de vente:"}]
        self.question_confirm_modif_type = [
            {"type": "confirm",
                     "name": "confirm_type",
                     "message": 'Souhaitez-vous modifier le type de bien?'}]
        self.question_modif_type = [
            {"type": "list",
                     "name": "modif_type",
                     "message": "Veuillez sélectionner le nouveau type de bien:",
                     "choices": ["Maison",
                                 "Appartement",
                                 "Dependance",
                                 "Local_Commercial"]}]
        self.question_confirm_modif_pprincipales = [
            {"type": "confirm",
                     "name": "confirm_pprincipales",
                     "message": 'Souhaitez-vous modifier le nombre de pièces principales?'}]
        self.question_modif_pprincipales = [
            {"type": "input",
                     "name": "modif_pprincipales",
                     "message": "Veuillez entrer le nouveau nombre de pièces principales:"}]
        self.question_confirm_modif_terrain = [
            {"type": "confirm",
                     "name": "confirm_terrain",
                     "message": 'Souhaitez-vous modifier la surface de terrain?'}]
        self.question_modif_terrain = [
            {"type": "input",
                     "name": "modif_terrain",
                     "message": "Veuillez entrer la nouvelle surface de votre terrain:"}]
        self.question_confirm_modif_surface = [
            {"type": "confirm",
                     "name": "confirm_surface",
                     "message": 'Souhaitez-vous modifier la surface réelle du bâti?'}]
        self.question_modif_surface = [
            {"type": "input",
                     "name": "modif_surface",
                     "message": "Veuillez entrer la nouvelle surface de votre bien:"}]

    def make_choice(self, user_info: str):
        annonces_user = user_info.get_annonces_client()
        choix = []
        compteur = 1
        for index, ligne in annonces_user.iterrows():
            mise_en_page_ligne = f"{compteur}. {ligne['Date_mutation']} | {ligne['Type_local']} | {ligne['Valeur_fonciere']} | {ligne['ref_annonce']}"
            choix.append(mise_en_page_ligne)
            compteur += 1
        choix.append("Retour au menu principal")
        questions = [
            {
                "type": "list",
                "name": "Liste Annonces",
                "message": "Quelle annonce souhaitez vous modifier ? \n"
                           " Date de publication | Type local |   Prix    | Ref. \n"
                           "----------------------------------------------------",
                "choices": choix,
            }]

        answers = prompt(questions)
        if answers['Liste Annonces'] == "Retour au menu principal":
            from project.view.client_view import ClientView
            return [ClientView(), user_info]
        else:
            dico_modif = {
                    'Date_mutation': None,
                    'Valeur_fonciere': None,
                    'Code_postal': None,
                    'Type_local': None,
                    'Surface_reelle_bati': None,
                    'Nombre_pieces_principales': None,
                    'Surface_terrain': None
                }

            # Confirmation et modification du code postal
            answer_confirm_cp = prompt(self.question_confirm_modif_cp)
            if answer_confirm_cp["confirm_cp"]:
                answer_modif_cp = prompt(self.question_modif_cp)
                transfo_code_postal = answer_modif_cp["modif_cp"][:2] + '_' + answer_modif_cp["modif_cp"][2:]
                dico_modif['Code_postal'] = transfo_code_postal

            # Confirmation et modification du prix de vente
            answer_confirm_prix = prompt(self.question_confirm_modif_prix)
            if answer_confirm_prix["confirm_prix"]:
                answer_modif_prix = prompt(self.question_modif_prix)
                dico_modif['Valeur_fonciere'] = float(answer_modif_prix["modif_prix"])

            # Confirmation et modification du type de bien
            answer_confirm_type = prompt(self.question_confirm_modif_type)
            if answer_confirm_type["confirm_type"]:
                answer_modif_type = prompt(self.question_modif_type)
                dico_modif['Type_local'] = str(answer_modif_type["modif_type"])

            # Confirmation et modification du nombre de pièces principales
            answer_confirm_pprincipales = prompt(self.question_confirm_modif_pprincipales)
            if answer_confirm_pprincipales["confirm_pprincipales"]:
                answer_modif_pprincipales = prompt(self.question_modif_pprincipales)
                dico_modif['Nombre_pieces_principales'] = int(answer_modif_pprincipales["modif_pprincipales"])

            # Confirmation et modification de la surface de terrain
            answer_confirm_terrain = prompt(self.question_confirm_modif_terrain)
            if answer_confirm_terrain["confirm_terrain"]:
                answer_modif_terrain = prompt(self.question_modif_terrain)
                dico_modif['Surface_terrain'] = float(answer_modif_terrain["modif_terrain"])

            # Confirmation et modification de la surface réelle du bâti
            answer_confirm_surface = prompt(self.question_confirm_modif_surface)
            if answer_confirm_surface["confirm_surface"]:
                answer_modif_surface = prompt(self.question_modif_surface)
                dico_modif['Surface_reelle_bati'] = float(answer_modif_surface["modif_surface"])

            refannonce = answers['Liste Annonces'].split("|")[-1].strip()
            Annonce.modifier_annonce(refannonce, dico_modif)
            return [ModifierView(), user_info]

    def display_info(self):
        print(Fore.BLUE + " MODIFICATION D'UNE ANNONCE ".center(80, "=") + Style.RESET_ALL)
