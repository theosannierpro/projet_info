"""Cette classe implémente la view EstimerView, qui correspond aux menus d'estimation
d'un bien"""

from datetime import date
from InquirerPy import prompt
from colorama import Fore, Style
from project.view.abstract_view import AbstractView
from project.enum_type import EnumType
from project.generateurs_id import generate_id_annonce
from project.annonce import Annonce


class EstimerView(AbstractView):
    """Classe de la view pour estimer un bien.

    Attributes
    --------------------
    questions_estim : list
        Question pour remplir les informations du bien à estimer
    question_post_estim : list
        Question sur l'action à réaliser après l'estimation
    question_autre_prix : list
        Question pour que l'utilisateur renseigne le prix de vente de son bien,
        le cas échéant.
    """
    def __init__(self):
        super().__init__()
        self.questions_estim = [
            {"type": "input",
                     "name": "cp",
                     "message": "Veuillez entrer le code postal du bien à estimer:"},
            {"type": "input",
                     "name": "pprincipales",
                     "message": "Veuillez entrer le nombre de pièces principales du bien à estimer:"},
            {"type": "list",
                     "name": "type",
                     "message": "Veuillez sélectionner le type du bien à estimer:",
                     "choices": ["Maison",
                                 "Appartement",
                                 "Dependance",
                                 "Local_Commercial"]},
            {"type": "input",
                     "name": "terrain",
                     "message": "Veuillez entrer la surface du terrain du bien à estimer:"},
            {"type": "input",
                     "name": "surface",
                     "message": "Veuillez entrer la surface du bien à estimer:"}
            ]
        self.question_post_estim = [
            {"type": "list",
                     "name": "post_estim",
                     "message": "Que souhaitez-vous faire désormais ?",
                     "choices": ["Poster une annonce avec ce prix",
                                 "Poster une annonce avec un autre prix",
                                 "Retour au menu principal"]}
            ]
        self.question_autre_prix = [
            {"type": "input",
                     "name": "autre_prix",
                     "message": "A quel prix souhaitez-vous poster cette annonce ?"}
            ]

    def make_choice(self, user_info: str):
        answers = prompt(self.questions_estim)
        print("Estimation de votre bien en cours ...")
        transfo_code_postal = answers["cp"][:2] + '_' + answers["cp"][2:]
        annonce_a_estimer = Annonce(
                    date_mise_en_vente=date.today(),
                    valeur_fonciere=float(999999999999),
                    code_postal=str(transfo_code_postal),
                    nb_pieces_principales=int(answers["pprincipales"]),
                    surface_terrain=float(answers["terrain"]),
                    surface_reelle_bati=float(answers["surface"]),
                    type_bien=EnumType[str(answers["type"])],
                    ref_annonce=generate_id_annonce(),
                    statut='A vendre',
                    client=user_info.id_client)
        prix_estime = annonce_a_estimer.estimer_annonce()
        if prix_estime != 999999999999:  # Valeur renvoyée si aucune estimation possible
            answer_post = prompt(self.question_post_estim)
            if answer_post["post_estim"] == "Poster une annonce avec ce prix":
                annonce_a_estimer.valeur_fonciere = prix_estime
                annonce_a_estimer.poster_annonce()
            elif answer_post["post_estim"] == "Poster une annonce avec un autre prix":
                autre_prix = prompt(self.question_autre_prix["autre_prix"])
                annonce_a_estimer.valeur_fonciere = autre_prix
                annonce_a_estimer.poster_annonce()
            elif answer_post["post_estim"] == "Retour au menu principal":
                pass
        from project.view.client_view import ClientView
        return [ClientView(), user_info]

    def display_info(self):
        print(Fore.BLUE + "ESTIMATION D'UNE ANNONCE ".center(80, "=") + Style.RESET_ALL)
