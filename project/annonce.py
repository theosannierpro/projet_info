"""Ce module implémente la classe Annonce"""

from datetime import date
from datetime import datetime
import pandas as pd
from colorama import Fore, Style
from project.enum_type import EnumType
from project.estimation_annonce import Estimation


class Annonce:
    """Représente une annonce immobilière.

    Représentation d'une annonce immobilière. Les informations sont présentes
    dans un dataframe et sont converties en une instance de cette classe pour
    être manipulées.

    Attributes:
    ---------------------
    date_mise_en_vente : datetime.date
            La date de mise en vente du bien immobilier.
    valeur_fonciere : float
            La valeur foncière de la propriété/ son prix de vente.
    code_postal : str
            Le code postal de la propriété.
    nb_pieces_principales : int
            Le nombre de pièces principales de la propriété.
    surface_terrain : float
            La surface du terrain de la propriété.
    surface_reelle_bati : float
            La surface habitable de la propriété.
    type_bien : EnumType
            Le type de bien immobilier.
    ref_annonce : str
            La référence de l'annonce.
    statut : str
            Le statut de l'annonce (par défaut : A vendre)
    client : str
            Le client propriétaire de l'annonce.

    Methods

    __str__() -> str
        Affiche les informations essentielles d'une annonce.

    estimer_annonce() -> float
        Estime le prix d'une annonce en connaissant certaines de ses informations.

    modifier_annonce(refannonce : str, dico_modif : dict) -> None
        Modifier les informations dans une annonce

    supprimer_annonce(refannonce : str) -> None
        Supprimer une annonce de la base de données

    poster_annonce() -> None
        Ajoute une nouvelle annonce dans la base de données
    ----------------------

    """
    def __init__(self,
                 date_mise_en_vente: datetime.date,
                 valeur_fonciere: float,
                 code_postal: str,
                 nb_pieces_principales: int,
                 surface_terrain: float,
                 surface_reelle_bati: float,
                 type_bien: EnumType,
                 ref_annonce: str,
                 statut: str,
                 client: str  # cf. import Client
                 ):
        self.date_mise_en_vente = date_mise_en_vente
        self.valeur_fonciere = valeur_fonciere
        self.code_postal = code_postal
        self.nb_pieces_principales = nb_pieces_principales
        self.surface_terrain = surface_terrain
        self.surface_reelle_bati = surface_reelle_bati
        self.type_bien = type_bien
        self.ref_annonce = ref_annonce
        self.client = client
        self.statut = statut

        if not isinstance(date_mise_en_vente, date):
            raise TypeError('La date de mise en vente doit être une '
                            'instance de datetime.datetime')
        if not isinstance(valeur_fonciere, float):
            raise TypeError('La valeur foncière doit être '
                            'une instance de float.')
        if not isinstance(code_postal, str):
            raise TypeError('Le code postal doit être une instance '
                            'de str.')
        if not isinstance(nb_pieces_principales, int):
            raise TypeError('Le nombre de pièces principales doit être '
                            'une instance de int.')
        if not isinstance(surface_terrain, float):
            raise TypeError('La surface du terrain doit être '
                            'une instance de float.')
        if not isinstance(surface_reelle_bati, float):
            raise TypeError('La surface réelle du bâti doit être '
                            'une instance de float.')
        if not isinstance(type_bien, EnumType):
            raise TypeError('Le type du bien doit être '
                            'une instance de EnumType.')
        if not isinstance(ref_annonce, str):
            raise TypeError("La référence de l'annonce doit être "
                            "une instance de str.")
        if not isinstance(client, str):
            raise TypeError('Le client doit être une instance '
                            'de str.')
        if not isinstance(statut, str):
            raise TypeError('Le statut doit être une instance '
                            'de str.')

    def __str__(self):
        """ Retourne une représentation sous forme de chaîne de caractères de l'annonce.

        Returns
        -------------------------
        str
            Chaîne de caractères représentant les informations de l'annonce.

        Examples
        --------------------------
        >>> annonce = Annonce(...)
        >>> print(annonce)
        Voici les informations concernant cette annonce:

        ============DETAILS PRATIQUES============
        Propriétaire: John Doe
        Date de mise en vente: 2024-05-01
        Code postal: 75001
        Statut: En vente
        Prix de vente: 250000.0
        Référence de l'annonce: ABC123
        ============PROPRIETES DU BIEN===========
        Type de bien: Appartement
        Nombre de pièces principales: 3
        Surface de terrain: 0.0 m²
        Surface réelle du bâti: 75.0 m²
        """
        display = f"""Voici les informations concernant cette annonce:\n
        ============DETAILS PRATIQUES============
        Propriétaire: {self.client}
        Date de mise en vente: {self.date_mise_en_vente}
        Code postal: {self.code_postal}
        Statut: {self.statut}
        Prix de vente: {self.valeur_fonciere}
        Référence de l'annonce: {self.ref_annonce}
        ============PROPRIETES DU BIEN===========
        Type de bien: {self.type_bien.name}
        Nombre de pièces principales: {self.nb_pieces_principales}
        Surface de terrain: {self.surface_terrain} m²
        Surface réelle du bâti: {self.surface_reelle_bati} m²\n"""
        return display

    def estimer_annonce(self):
        """Estime le prix d'une annonce

        Le prix de l'annonce est estimé à l'aide des annonces déjà postées.
        Le département, le type de bien et sa superficie sont pris en compte
        pour l'estimation.

        Returns
        -------------------------
        float
            Prix estimé de l'annonce (=999999999999 si pas d'estimation possible)

        Examples
        --------------------------
        >>> annonce = Annonce(...)
        >>> annonce.estimer_annonce()
        200000.0
        """
        bdd_annonces = pd.read_csv('resources/base_de_donnees.csv')
        Estimation.update_bdd_estimation(bdd_annonces)
        bdd_estimation = pd.read_csv('resources/bdd_estimation.csv')
        cp = self.code_postal
        departement_annonce = int(cp[0:2])
        prix_moyen_m2 = bdd_estimation[
            (bdd_estimation['departement'] == departement_annonce) &
            (bdd_estimation['Type_local'] == str(self.type_bien.name))]['Prix_m2']
        if len(prix_moyen_m2) > 0:
            prix_estimation = prix_moyen_m2.iloc[0] * (self.surface_reelle_bati + 0.2 * self.surface_terrain)
            print("Votre bien est estimé à", prix_estimation, " €")
            return prix_estimation

        print(Fore.RED + "Nous ne disposons pas de suffisamment de données "
              "pour estimer votre bien." + Style.RESET_ALL)
        return 999999999999

    @staticmethod
    def modifier_annonce(refannonce, dico_modif):
        """Modifie une annonce

        Parameters
        -------------------------
        refannonce : str
            Référence de l'annonce dans la base de données à modifier.
        dico_modif : dict
            Dictionnaire contenant les modifications à apporter à l'annonce. Les
            valeurs valent None si aucune modification n'est à apporter.

        Returns
        -------------------------
        None

        Examples
        --------------------------
        >>> annonce = Annonce(...)
        >>> annonce.modifier_annonce('ABC123', {'statut': 'Vendu', 'prix': 230000.0})
        Annonce modifiée avec succès !
        """
        bdd_annonces = pd.read_csv('resources/base_de_donnees.csv')
        for key, value in dico_modif.items():
            if value is not None:
                index_ligne_a_modif = bdd_annonces.index[
                    bdd_annonces['ref_annonce'] == refannonce][0]
                bdd_annonces.loc[index_ligne_a_modif, key] = value
        bdd_annonces.to_csv('resources/base_de_donnees.csv')
        print(Fore.GREEN + "Annonce modifiée avec succès !" + Style.RESET_ALL)

    @staticmethod
    def supprimer_annonce(refannonce):
        """Supprime une annonce de la base de données.

        Parameters
        -------------------------
        refannonce : str
            Référence de l'annonce à supprimer.

        Returns
        -------------------------
        None

        Examples
        --------------------------
        >>> annonce = Annonce(...)
        >>> Annonce.supprimer_annonce('ABC123')
        Annonce supprimée avec succès !
        """
        bdd_annonces = pd.read_csv('resources/base_de_donnees.csv')
        ligne_a_suppr = bdd_annonces.query('ref_annonce == @refannonce')
        annonce_a_suppr = Annonce(
            date_mise_en_vente=datetime.strptime(ligne_a_suppr['Date_mutation'].iloc[0], "%Y-%m-%d").date(),
            valeur_fonciere=ligne_a_suppr['Valeur_fonciere'].iloc[0],
            code_postal=str(ligne_a_suppr['Code_postal'].iloc[0]),
            nb_pieces_principales=int(ligne_a_suppr['Nombre_pieces_principales'].iloc[0]),
            surface_terrain=float(ligne_a_suppr['Surface_terrain'].iloc[0]),
            surface_reelle_bati=ligne_a_suppr['Surface_reelle_bati'].iloc[0],
            type_bien=EnumType[str(ligne_a_suppr['Type_local'].iloc[0])],
            ref_annonce=ligne_a_suppr['ref_annonce'].iloc[0],
            statut=ligne_a_suppr['Statut'].iloc[0],
            client=ligne_a_suppr['Client'].iloc[0])
        bdd_annonces_apres_suppr = bdd_annonces[bdd_annonces['ref_annonce'] != annonce_a_suppr.ref_annonce]
        bdd_annonces_apres_suppr.to_csv('resources/base_de_donnees.csv', index=False)
        bdd_annonces = bdd_annonces_apres_suppr
        print(Fore.GREEN + 'Annonce supprimée avec succès !' + Style.RESET_ALL)

    def poster_annonce(self):
        """Ajoute une nouvelle annonce.

        Cet ajout se traduit par l'écriture d'une nouvelle ligne dans la base
        de données.

        Returns
        -------------------------
        None

        Examples
        --------------------------
        >>> annonce = Annonce(...)
        >>> annonce.poster_annonce()
        Annonce postée avec succès ! Référence : ABC123.
        """
        new_row_bdd = pd.DataFrame({
                     'Date_mutation': [self.date_mise_en_vente],
                     'Valeur_fonciere': [self.valeur_fonciere],
                     'Code_postal': [self.code_postal],
                     'Type_local': [self.type_bien.name],
                     'Surface_reelle_bati': [self.surface_reelle_bati],
                     'Nombre_pieces_principales': [self.nb_pieces_principales],
                     'Surface_terrain': [self.surface_terrain],
                     'Client': [self.client],
                     'ref_annonce': [self.ref_annonce],
                     'Statut': [self.statut]
                     })
        new_line = '\n'+','.join(map(str, new_row_bdd.iloc[0].tolist()))
        with open('resources/base_de_donnees.csv', 'a', encoding='utf-8') as f:
            f.write(new_line)
        bdd_annonces = pd.read_csv('resources/base_de_donnees.csv')
        print(Fore.GREEN + f"Annonce postée avec succès ! Référence : {self.ref_annonce}." + Style.RESET_ALL)
