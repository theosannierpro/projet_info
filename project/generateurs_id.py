"""Ce module contient des fonctions pour générer des identifiants uniques pour les clients
et les annonces"""

import random
import string


def generate_id_client():
    """Génère un identifiant pour un client.

    Identifiant de la forme XYZT où X,Y,Z,T sont des caractères alphanumériques

    Returns:
    -------------------------
    str
        Identifiant (supposé unique) généré pour un client.

    Examples
    --------------------------
    >>> generate_id_client()
    '2h8A'
    """
    generation = ''.join(random.choices(string.ascii_letters + string.digits,
                                        k=4))
    unique_id = f"{generation}"
    return unique_id


def generate_id_annonce():
    """Génère un identifiant unique pour une annonce.

    Identifiant de la forme XYZTUV
    où X,Y,Z,T,U,V sont des caractères alphanumériques

    Returns:
    -------------------------
    str
        Identifiant (supposé unique) généré pour une annonce.

    Examples
    --------------------------
    >>> generate_id_annonce()
    '2h8A'
    """
    generation = ''.join(random.choices(string.ascii_letters + string.digits,
                                        k=6))
    unique_id = f"{generation}"
    return unique_id
