"""Ce module implémente la classe Personne, de laquelle hérite la classe Client"""

class Personne:
    """Représente une personne avec son prénom, son nom et son âge.

    Cette classe permet de représenter une personne avec son prénom, son nom
    et son âge.

    Attributes:
    ---------------------
    prenom : str
        Le prénom de la personne.
    nom : str
        Le nom de la personne.
    age : int
        L'âge de la personne.
    
    Methods
    --------------------
    __str__() -> str
        Implémente une manière informelle d'afficher une Personne

    """
    def __init__(self, prenom: str, nom: str, age: int):
        self.prenom = prenom
        self.nom = nom
        self.age = age
        if not isinstance(prenom, str):
            raise TypeError("Le prénom doit être un str.")
        if not isinstance(nom, str):
            raise TypeError("Le nom doit être un str.")
        if not isinstance(age, int):
            raise TypeError("L'âge doit être un entier.")
        if not age > 0:
            raise ValueError("L'âge doit être strictement positif.")

    def __str__(self):
        """Retourne une représentation sous forme de chaîne de caractères de la personne.

        Returns:
        -------------------------
        str
            Chaîne de caractères représentant les informations de la personne.

        """
        return f"Nom: {self.nom}\nPrenom: {self.prenom}\nAge: {self.age}"
