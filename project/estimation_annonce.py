""""Ce module implémente des fonctions nécessaires à l'estimation de biens immobiliers"""

import pandas as pd


class Estimation:
    """
    Classe contenant des méthodes pour estimer le prix des biens immobiliers.

    Methods
    -------------------------
    get_prix_m2(bdd_annonces : pandas.DataFrame, type_local: EnumType, dep: str)
        Calcule le prix moyen au mètre carré pour un type de bien et un département donné.

    update_bdd_estimation(bdd_annonces : pandas.DataFrame)
        Met à jour la base de données d'estimation des prix au mètre carré
        pour chaque type de bien et chaque département.
    """
    def __init__(self):
        pass

    @staticmethod
    def get_prix_m2(bdd_annonces, type_local, dep):
        """
        Calcule le prix moyen au mètre carré pour un type de bien et un département donnés.

        Parameters
        -------------------------
        bdd_annonces : pandas.DataFrame
            DataFrame contenant les données des annonces immobilières.
        type_local : EnumType
            Type de bien immobilier (Maison, Appartement, Dépendance, Local Commercial).
        dep : str
            Département pour lequel le prix moyen du m² est calculé.

        Returns
        -------------------------
        float
            Prix moyen au mètre carré pour le type de bien et le département donnés.
        """
        bdd_filtrees = bdd_annonces[(bdd_annonces['Type_local'] == type_local) & (bdd_annonces['Code_postal'].str.startswith(dep))]
        prix_total = bdd_filtrees['Valeur_fonciere'].sum()
        surface_totale = bdd_filtrees['Surface_reelle_bati'].sum() + bdd_filtrees['Surface_terrain'].sum() * 0.2
        prix_moyen_m2 = prix_total / surface_totale
        return prix_moyen_m2

    @staticmethod
    def update_bdd_estimation(bdd_annonces):
        """
        Met à jour la base de données des prix au mètre carré pour chaque type 
        de bien et chaque département.

        Cette méthode est appellée à chaque estimation afin que les prix au m² 
        prennent en compte les éventuelles annonces (ou celles supprimées).
        Les prix sont alors tenus à jour.

        Parameters
        -------------------------
        bdd_annonces : pandas.DataFrame
            DataFrame contenant les données des annonces immobilières.

        Returns
        -------------------------
        None
        """
        bdd_annonces = pd.read_csv('resources/base_de_donnees.csv')
        df_estim = pd.DataFrame()
        departements = bdd_annonces['Code_postal'].str.slice(stop=2).unique()
        type_local = list(bdd_annonces['Type_local'].unique())
        nouvelle_ligne = {'departement': None, 'Type_local': None, 'Prix_m2': None}
        for dep in departements:
            for t_loc in type_local:
                nouvelle_ligne['departement'] = dep
                nouvelle_ligne['Type_local'] = t_loc
                nouvelle_ligne['Prix_m2'] = round(Estimation.get_prix_m2(bdd_annonces, t_loc, dep), 2)
                df_estim = df_estim.append(nouvelle_ligne, ignore_index=True)
        df_estim['departement'] = df_estim['departement'].astype(str)
        df_estim['Type_local'] = df_estim['Type_local'].astype(str)
        df_estim['Prix_m2'] = df_estim['Prix_m2'].astype(float)
        print(df_estim.dtypes)
        df_estim.to_csv('resources/BDD_estimation.csv', index=False)
