import re
import pytest
from project.personne import Personne


@pytest.mark.parametrize(
    "prenom, nom, age, expected_error, error_type",
    [
        (["John"], "Doe", 10, "Le prénom doit être un str.", TypeError),
        ("John", ["Doe"], 10, "Le nom doit être un str.", TypeError),
        ("John", "Doe", 0, "L'âge doit être strictement positif.", ValueError),
        ("John", "Doe", 10.1, "L'âge doit être un entier.", TypeError)
    ],
)
def test_personne_init_echec(prenom, nom, age, expected_error, error_type):
    with pytest.raises(error_type, match=re.escape(expected_error)):
        Personne(prenom, nom, age)
