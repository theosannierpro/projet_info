from unittest import mock
from datetime import date
import re
import pytest
import pandas as pd
from project.client import Client
from project.enum_type import EnumType


@pytest.mark.parametrize(
    "prenom, nom, age, id_client, expected_error, error_type",
    [
        ("John", "Doe", 10, 1283263,
         "L'identifiant client doit être une instance de string.", TypeError),
    ],
)
def test_client_init_echec(prenom, nom, age, id_client,
                           expected_error, error_type):
    with pytest.raises(error_type, match=re.escape(expected_error)):
        Client(prenom, nom, age, id_client)

# Test de la méthode get_annonces_client


def test_get_annonces_client():
    # Données simulées pour le CSV

    data = [
        {
            "Date_mutation": date(2023, 10, 1),
            "Valeur_fonciere": 500000.0,
            "Code_postal": "75001",
            "Type_local": EnumType["Maison"].name,
            "Surface_reelle_bati": 200.0,
            "Nombre_pieces_principales": 3,
            "Surface_terrain": 1000.0,
            "Client": "12345",
            "ref_annonce": "REF1234",
            "Statut": "En vente"
        },
        {
            "Date_mutation": date(2023, 11, 15),
            "Valeur_fonciere": 750000.0,
            "Code_postal": "75002",
            "Type_local": EnumType["Appartement"].name,
            "Surface_reelle_bati": 250.0,
            "Nombre_pieces_principales": 4,
            "Surface_terrain": 1500.0,
            "Client": "67890",
            "ref_annonce": "REF5678",
            "Statut": "En vente"
        },
        {
            "Date_mutation": date(2024, 1, 5),
            "Valeur_fonciere": 600000.0,
            "Code_postal": "75003",
            "Type_local": EnumType["Maison"].name,
            "Surface_reelle_bati": 300.0,
            "Nombre_pieces_principales": 5,
            "Surface_terrain": 1200.0,
            "Client": "12345",
            "ref_annonce": "REF7890",
            "Statut": "En vente"
        },
    ]
    # Création du DataFrame simulé
    df = pd.DataFrame(data)

    # On simule la lecture du fichier CSV pour retourner notre DataFrame
    with mock.patch('pandas.read_csv', return_value=df):
        client = Client('John', 'Doe', 30, '12345')

        # Appel de la méthode à tester
        result = client.get_annonces_client()

        # Vérification du contenu du résultat
        assert len(result) == 2
        clients = result["Client"].values
        assert all(client == '12345' for client in clients)


def test_creer_compte_ajout_ligne():
    # Simule la génération d'un identifiant fixe pour le client
    with mock.patch('project.generateurs_id.generate_id_client',
                    return_value='XYZ1234'):
        # Simule l'ouverture d'un fichier CSV pour l'écriture
        with mock.patch('builtins.open', mock.mock_open()) as mock_file:
            # Crée un client avec des détails de test
            client = Client('Alice', 'Smith', 25, '')

            # Appel de la méthode creer_compte
            client.creer_compte()

            # Récupère le handle du mock
            handle = mock_file()

            # Vérifie que le fichier a été ouvert en mode d'ajout
            mock_file.assert_any_call('resources/bdd_client.csv', 'a',
                                      encoding='utf-8')

            # Vérifie si une ligne a été ajoutée
            handle.write.assert_called_once()
            # Assure qu'un seul appel à write a eu lieu

            written_data = handle.write.call_args[0][0]
            # Récupère le contenu écrit
            assert 'Alice' in written_data
            assert 'Smith' in written_data
            assert '25' in written_data  # Âge attendu
