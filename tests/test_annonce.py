from unittest import mock
from datetime import date
from datetime import datetime
import re
import pytest
import pandas as pd
from project.enum_type import EnumType
from project.annonce import Annonce



@pytest.mark.parametrize(
    "date_mise_en_vente, valeur_fonciere, code_postal, nb_pieces_principales, "
    "surface_terrain, surface_reelle_bati, type_bien, ref_annonce, statut,"
    "client, expected_error, error_type",
    [
        ("2023-10-01", 500000.0, "75001", 3, 1000.0, 200.0,
         EnumType["Maison"],
         "REF1234", "En vente", "Client123",
         "La date de mise en vente doit être une instance"
         " de datetime.datetime",
         TypeError),

        (date(2023, 10, 1), "500000", "75001", 3, 1000.0, 200.0,
         EnumType["Maison"],
         "REF1234", "En vente", "Client123",
         "La valeur foncière doit être une instance de float.",
         TypeError),

        (date(2023, 10, 1), 500000.0, 75001, 3, 1000.0, 200.0,
         EnumType["Maison"],
         "REF1234", "En vente", "Client123",
         "Le code postal doit être une instance de str.",
         TypeError),

        (date(2023, 10, 1), 500000.0, "75001", "trois", 1000.0, 200.0,
         EnumType["Maison"],
         "REF1234", "En vente", "Client123",
         "Le nombre de pièces principales doit être une instance de int.",
         TypeError),

        (date(2023, 10, 1), 500000.0, "75001", 3, {1000}, 200.0,
         EnumType["Maison"],
         "REF1234", "En vente", "Client123",
         "La surface du terrain doit être une instance de float.",
         TypeError),

        (date(2023, 10, 1), 500000.0, "75001", 3, 1000.0, "200",
         EnumType["Maison"],
         "REF1234", "En vente", "Client123",
         "La surface réelle du bâti doit être une instance de float.",
         TypeError),

        (date(2023, 10, 1), 500000.0, "75001", 3, 1000.0, 200.0, "Maison",
         "REF1234", "En vente", "Client123",
         "Le type du bien doit être une instance de EnumType.",
         TypeError),

        (date(2023, 10, 1), 500000.0, "75001", 3, 1000.0, 200.0,
         EnumType["Maison"],
         1234, "En vente", "Client123",
         "La référence de l'annonce doit être une instance de str.",
         TypeError),

        (date(2023, 10, 1), 500000.0, "75001", 3, 1000.0, 200.0,
         EnumType["Maison"],
         "REF1234", 100, "Client123",
         "Le statut doit être une instance de str.",
         TypeError),

        (date(2023, 10, 1), 500000.0, "75001", 3, 1000.0, 200.0,
         EnumType["Maison"],
         "REF1234", "En vente", 1234,
         "Le client doit être une instance de str.",
         TypeError),
    ]
)
def test_annonce_init_echec(date_mise_en_vente, valeur_fonciere, code_postal,
                            nb_pieces_principales, surface_terrain,
                            surface_reelle_bati, type_bien, ref_annonce,
                            statut, client, expected_error, error_type):
    with pytest.raises(error_type, match=re.escape(expected_error)):
        Annonce(date_mise_en_vente, valeur_fonciere, code_postal,
                nb_pieces_principales, surface_terrain, surface_reelle_bati,
                type_bien, ref_annonce, statut, client)


@pytest.mark.parametrize(
    "date_mise_en_vente, valeur_fonciere, code_postal, nb_pieces_principales, "
    "surface_terrain, surface_reelle_bati, type_bien, ref_annonce, statut, "
    "client",
    [
        (date(2023, 10, 1), 500000.0, "75001", 3, 1000.0, 200.0, EnumType[
            "Maison"], "REF1234", "En vente", "Client123"),
    ]
)
def test_annonce_init_succes(date_mise_en_vente, valeur_fonciere, code_postal,
                             nb_pieces_principales, surface_terrain,
                             surface_reelle_bati, type_bien, ref_annonce,
                             statut, client):
    Annonce(date_mise_en_vente, valeur_fonciere, code_postal,
            nb_pieces_principales, surface_terrain, surface_reelle_bati,
            type_bien, ref_annonce, statut, client)


def test_poster_annonce(capsys):
    # Simule un fichier CSV
    initial_data = (
        'Date_mutation,Valeur_fonciere,Code_postal,Type_local,'
        'Surface_reelle_bati,Nombre_pieces_principales,Surface_terrain,Client,'
        'ref_annonce,Statut\n')

    # Simule l'ouverture d'un fichier CSV
    with mock.patch(
        'builtins.open', mock.mock_open(read_data=initial_data)
         ) as mock_file:
        # Crée une annonce
        annonce = Annonce(
            date_mise_en_vente=datetime(2023, 10, 1).date(),
            valeur_fonciere=500000.0,
            code_postal='75001',
            nb_pieces_principales=3,
            surface_terrain=1000.0,
            surface_reelle_bati=200.0,
            type_bien=EnumType["Maison"],
            ref_annonce='REF1234',
            statut='En vente',
            client='12345'
        )

        # Appel de la méthode poster_annonce
        annonce.poster_annonce()

        # Récupère le handle du mock
        handle = mock_file()

        # Vérifie que le fichier a été ouvert pour écriture
        mock_file.assert_any_call(
            'resources/base_de_donnees.csv', 'a', encoding='utf-8'
            )

        # Vérifie le contenu écrit dans le fichier
        expected_line = ('\n2023-10-01,500000.0,75001,Maison,200.0,3,1000.0,'
                         '12345,REF1234,En vente')
        handle.write.assert_called_once_with(expected_line)

        # Capture de la sortie console avec capsys
        captured = capsys.readouterr()

        # Vérifie le message imprimé
        assert (
            "Annonce postée avec succès ! Référence : REF1234." in captured.out
            )


def test_supprimer_annonce(capsys):
    # Données simulées pour le CSV
    data = {
        'ref_annonce': ['REF1234', 'REF5678'],
        'Date_mutation': ['2023-10-01', '2023-10-15'],
        'Valeur_fonciere': [500000.0, 750000.0],
        'Code_postal': ['75001', '75002'],
        'Type_local': ['Maison', 'Appartement'],
        'Surface_reelle_bati': [200.0, 250.0],
        'Nombre_pieces_principales': [3, 4],
        'Surface_terrain': [1000.0, 1500.0],
        'Client': ['12345', '67890'],
        'Statut': ['En vente', 'Vendu']
    }

    df = pd.DataFrame(data)

    # Simule la lecture d'un fichier CSV pour obtenir les annonces
    with mock.patch('pandas.read_csv', return_value=df):
        # Simule l'ouverture d'un fichier CSV pour l'écriture
        with mock.patch('builtins.open', mock.mock_open()) as mock_file:
            Annonce.supprimer_annonce('REF1234')

            # Récupère le handle du mock
            handle = mock_file()

            handle.write.assert_called_with(
                'REF5678,2023-10-15,750000.0,75002,Appartement,250.0,4,1500.0,'
                '67890,Vendu\r\n')


def test_modifier_annonce(capsys):
    # Données simulées pour le CSV
    data = {
        'ref_annonce': ['REF1234', 'REF5678'],
        'Date_mutation': ['2023-10-01', '2023-10-15'],
        'Valeur_fonciere': [500000.0, 750000.0],
        'Code_postal': ['75001', '75002'],
        'Type_local': ['Maison', 'Appartement'],
        'Surface_reelle_bati': [200.0, 250.0],
        'Nombre_pieces_principales': [3, 4],
        'Surface_terrain': [1000.0, 1500.0],
        'Client': ['12345', '67890'],
        'Statut': ['En vente', 'Vendu']
    }

    df = pd.DataFrame(data)

    # Simule la lecture du CSV pour modifier les annonces
    with mock.patch('pandas.read_csv', return_value=df):
        # Simule l'ouverture d'un fichier CSV pour l'écriture
        with mock.patch('builtins.open', mock.mock_open()) as mock_file:
            Annonce.modifier_annonce('REF1234', {'Valeur_fonciere': 550000.0})
            captured = capsys.readouterr()
            assert "Annonce modifiée avec succès !" in captured.out
