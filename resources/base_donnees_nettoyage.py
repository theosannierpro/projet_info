import sys
import os
import pandas as pd
import numpy as np
# Régler les problèmes d'imports

# Ajouter le répertoire racine au PYTHONPATH
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))

from project.generateurs_id import generate_id_client
from project.generateurs_id import generate_id_annonce

data = pd.read_csv(
    r"C:\Users\ninot\OneDrive\Bureau\ENSAI\base_donnees.csv"
    )

data = data[['Date.mutation', 'Valeur.fonciere', 'Code.postal',
             'Type.local', 'Surface.reelle.bati',
             'Nombre.pieces.principales', 'Surface.terrain']]

colonnes = ['Date.mutation', 'Valeur.fonciere', 'Surface.reelle.bati',
            'Code.postal', 'Nombre.pieces.principales']

# On enlève les NA sur ces colonnes
data = data.dropna(subset=colonnes)

# Renommer les colonnes

data.columns = data.columns.str.replace('.', '_')

# On enleve l'accent sur la modalité dépendance
data["Type_local"] = data["Type_local"].replace("Dépendance", "Dependance")

# Ajout d'une colonne client et des identifiants

data["Client"] = [generate_id_client() for _ in range(len(data))]

# Création de la colonne ref_annonce et des identifiants correspondants

data["ref_annonce"] = [generate_id_annonce() for _ in range(len(data))]

# Passage du type str à type float
# Nettoyage des virgules
data["Valeur_fonciere"] = data["Valeur_fonciere"].str.replace(",", ".")
data["Valeur_fonciere"] = data["Valeur_fonciere"].astype(float)

# Modification de la colonne Code_postal

# On ajoute un 0 devant les codes postaux de longueurs 4
# On crée une fonction qu'on va appliquer à la colonne Code_postal du df

# On transforme en int pour se débarasser des 0 derrière et on transforme
# en str par la même occasion

data["Code_postal"] = data["Code_postal"].apply(str)

data["Code_postal"] = data["Code_postal"].replace(r'\.0$', '', regex=True).str.strip()

def ajouter_zero(str):
    if len(str) == 4:
       return '0' + str
    # Si le code postal est de longueur 5, c'est bon
    return str


data["Code_postal"] = data["Code_postal"].apply(ajouter_zero)

def modif_code_postal(str):
    segments = list(str)
    segments.insert(2, '_')
    code_modifie = ''.join(segments)
    return code_modifie

data["Code_postal"] = data["Code_postal"].apply(modif_code_postal)



# Transformation de la colonne Date_mutation au format date.datetime

data["Date_mutation"] = pd.to_datetime(data["Date_mutation"])

# Type Nombre pièces principales : int

data["Nombre_pieces_principales"] = data["Nombre_pieces_principales"
                                          ].astype(int)

# Type Surface_reelle_bati : float

data["Surface_reelle_bati"] = data["Surface_reelle_bati"].astype(float)

# Type Surfec terrain : float

data["Surface_terrain"] = data["Surface_terrain"].fillna(0)
data["Surface_terrain"] = data["Surface_terrain"].astype(float)

# Ajout d'une colonne Statut qui peux prendre les modalités
# A vendre ou Vendu
# Pour l'instant, tous les biens sont à vendre.

data["Statut"] = "A vendre"



# Pour rendre la base de données moins volumineuse, on supprime
# 10% des lignes de manière aléatoire

# Sélection des lignes à conserver

data = data.sample(frac=0.1, random_state=42)

# Changement Local industriel ect en local commercial

data["Type_local"] = data["Type_local"].replace(
    "Local industriel. commercial ou assimilé", "Local_Commercial")

# Supression des NA restants
data.replace(np.nan, 'NC', inplace=True)

data.to_csv('resources/base_de_donnees.csv', index=False)
