import pandas as pd

bdd_client = pd.DataFrame({"nom": "Dupond",
                           "prenom": "Dupont",
                           "age": 37,
                           "client_id": "1234"},
                          index=[1])
print(bdd_client)
bdd_client.to_csv('bdd_client.csv', index=False)
