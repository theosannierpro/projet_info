from project.view.accueil_view import AccueilView

User_info = ''  # Initialisation de User qui contiendra les infos de la personne
                # en train d'utiliser l'application


view = AccueilView()

with open("resources/banner.txt", mode="r", encoding="utf-8") as title:
    print(title.read())

while view:
    view.display_info()
    view, User_info = view.make_choice(User_info)

with open("resources/exit.txt", mode="r", encoding="utf-8") as exit_message:
    print(exit_message.read())
